# Firing Squad

Ce projet a pour but de trouver une solution pour le problème du "Firing Squad" a 4 états.

## Installation

### Clonage du projet

```
git clone https://gitlab.com/zohra051/firing-squad
```

### Construction et compilation

Entrez dans le dossier cloné puis :

```
mkdir build 
cd build
cmake ..
make
```

## Utilisation

Il existe deux mode d'utilisation. 

 * Iterated Local Search
 * Algorithme Evolutionnaire

dans les deux cas, il faut se placer dans le dossier build, et exécuté la commande détaillée plus bas.

Le résultat se place dans la racine du projet dans un fichier nommé "result.txt".

/!\ si vous voulez préserver un résultat, sauvegarder le fichier sous un autre nom, le programme écrase le fichier result.txt à chaque éxécution. /!\ 

### Iterated Local Search


```
./main <iter_fi> <iter_ils> <pert> 0
```

* iter_fi : nombre d'évaluation fait dans la recherche local
* iter_ils : nombre d'itération de recherche local
* pert : nombre de regle perturber

### Algorithme Evolutionnaire


```
./main <pop> <vie> <gen> 1
```

* pop : Nombre d'individu
* vie : espérence de vie d'un individu (nb d'itération hill-climber)
* gen : nombre de génération

## Auteur

Ce projet a été réalisé par Harrat Zohra & Denis Jimmy.

Sous la supervision de M.Verel . 
