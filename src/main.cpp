#include "Fusillier.hpp"
#include <iostream>
#include <random>
#include <cstdlib>

using namespace std;


int main (int argc,char* argv[])
{

	if(argc != 5)
	{
		cout<<"comment l'utiliser:   ./main <iter_fi> <iter_ils> <pert> <mode>"<<endl;
		cout<<"\t en mode 0 = Iterated Local Search"<<endl;
		cout<<"\t iter_fi : nombre d'évaluation fait dans la recherche local"<<endl;
		cout<<"\t iter_ils : nombre d'itération de recherche local"<<endl;
		cout<<"\t pert : nombre de regle perturber"<<endl;
		cout<<endl;
		cout<<"                      ./main <pop> <vie> <gen> <mode>"<<endl;
		cout<<"\t en mode 1 = Algorithme Evolutionnaire"<<endl;
		cout<<"\t pop : Nombre d'individue"<<endl;
		cout<<"\t vie : espérence de vie d'un individu (nb d'itération hill-climbre)"<<endl;
		cout<<"\t gen : nombre de génération"<<endl;
		return 1;
	}
	srand (time(NULL));
	Automata automate(20);
	Fusiller fusillier(automate);
	fusillier.print_regle();
	fusillier.print_eval();
	cout<<endl;
	if(stoi(argv[4]) == 0)
		fusillier.ILS(stoi(argv[1]),stoi(argv[2]),stoi(argv[3]));
	else if(stoi(argv[4]) == 1)
		fusillier.algo_evolution(stoi(argv[1]),stoi(argv[2]),stoi(argv[3]));
	else
	{
		cout<<"Le mode sélectionner n'est pas le bon ! "<<endl;
		cout<<"\t mode : 0 = Iterated Local Search"<<endl;
		cout<<"\t        1 = Algorithme Evolutionnaire"<<endl;
		return 1;
	}
	cout<<endl;
	fusillier.print_best_solution();
	fusillier.print_eval();
	fusillier.write();
	return 0;
}


//100000 10000 2
//15
//63m6,725s
//14
//66m56,096s
//14
//67m7,656s
//14
//62m13,609s
//15
//user	62m18,384s
//14
//real	63m15,941s




//100000 10000 3
//15
//59m11,167s
//15
//real	62m6,165s
//14
//real	58m34,947s
//15
//real	60m13,039s
//14
//real	58m30,486s
//15
//real	58m12,010s


//100000 10000 11
//14
//57m19,866s
//15
//50m25,305s
//14
//55m20,771s
//14
//48m41,007s
//14
//48m29,049s
//14
//51m20,724s



//100000 100000 3
//15
//684m2,336s


