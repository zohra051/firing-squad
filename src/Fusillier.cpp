#include "Fusillier.hpp"
#define MAX 10000
//1000000 pour 7
//nombre tentative avec 8 : 3 + celle en cours
const unsigned int n =6;

using namespace std;

Regle initRules(char * fileName) {
  fstream file(fileName, ios::in);

  if (file.is_open()) {
    // 5 états + l'état "bord"
    unsigned n = 5 + 1;

    Regle rules = new unsigned[n * n * n];

    unsigned value;
    for(unsigned i = 0; i < n * n * n; i++) {
      file >> value;
      rules[i] = value;
    }

    return rules;
  } else
    cerr << "Impossible d'ouvrir " << fileName << endl;
}


Fusiller::Fusiller(Automata _automata):
automata(_automata)
{
	//regle = initRules("../meilleur_12.dat");
	indice_utile();
	regle = initialisation();
	voisins = new unsigned[n*n*n];
	best_regle = new unsigned[n*n*n];
	best_regle_update();
}




Regle Fusiller::initialisation ()
{
	Regle regles = new unsigned[n*n*n];
	for(unsigned int i=0; i<n*n*n;i++)
	{
		regles[i] = 5;
	}


	/*Tableau de règle aléatoire*/
	for(unsigned int i:utile)
	{
		regles[i] = rand()%4;
	}
	//000
	regles[0] = 0;
	//00B
	regles[5] = 0;
	//B00
	regles[180] = 0;
	//111
	regles[43] = 4;
	//B11
	regles[187] = 4;
	//11B
	regles[47] = 4;
	//10B
	regles[41] = 1;
	//B10
	regles[186] = 1;
	return regles;
}

void Fusiller::indice_utile ()
{
	for(int i=0;i<6;i++)
	{
		if(i!=4)
		{
			for(int j=0; j<4;j++)
			{		for(int k=0; k<6;k++)
					{
						if(k!=4 && !(i==5 && k==5))
						{
							utile.push_back(i*36+j*6+k);
						}
					}
			}
		}
	}

	utile.erase(utile.begin() + 85);
	utile.erase(utile.begin() + 84);
	utile.erase(utile.begin() + 80);
	utile.erase(utile.begin() + 29);
	utile.erase(utile.begin() + 26);
	utile.erase(utile.begin() + 24);
	utile.erase(utile.begin() + 4);
	utile.erase(utile.begin() + 0);
}

void Fusiller::voisin()
{
	//on y copy notre solution initiale
	copy(regle,regle+ n*n*n,voisins);
	//on choisie qu'elle règle on modifie
	int position_regle = utile[rand()%utile.size()];
	//on choisie qu'elle règle on va assigner
	int numero_regle = rand()%4;
	//si le numero_regle choisie et la même que celle de la solution initiale, on en choisie une autre
	if(numero_regle == regle[utile[position_regle]])
	{
		numero_regle = (numero_regle +1)%4;
	}
	//affectation de la nouvelle règle
	voisins[position_regle] = numero_regle;
}

void Fusiller::first_improvement(int iter)
{

	int cpt = 0;
	while(cpt < iter)
	{
		int init = automata.f(regle,20); //evaluer notre solution initiale
		//on prend un voisin aleatoire
		voisin();
		//on evalue
		int eval_voisin = automata.f(voisins,20);
		//on augmente le nb d'eval
		if(init == eval_voisin)
			cpt++;
		//si meuilleur
		if(eval_voisin >= init)
		{
			//notre voisin devient la solution initiale
			swap(voisins,regle);
		}

	}
}

void Fusiller::ILS(int fi_iteration,int ils_iteration,int perturbation_nb)
{
	first_improvement(fi_iteration);
	best_regle_update();
	int cpt = 0;
	while(cpt < ils_iteration)
	{
		perturbation(perturbation_nb);
		first_improvement(fi_iteration);
		//cout<<automata.f(regle,20)<<" ";
		if(automata.f(best_regle,20) <= automata.f(regle,20))
		{
			best_regle_update();
			print_best_solution();
			print_eval();
		}
		cpt++;
	}
	cout<<endl;
}

void Fusiller::perturbation(int pert)
{
	for(int i=0; i<pert; i++)
	{
		voisin();
		swap(voisins,regle);
	}
}

void Fusiller::algo_evolution(int population,int iteration,int generation)
{
	int pop = population;
	int iter =iteration;
	int max = generation;

	//initialisation
	init_evolution(pop,iter);

	//boucle
	int cpt = 0;
	while(cpt <max)
	{
		cpt++;
		//garde en mémoire le meilleur
		if(eval_Parent[pop-1][0] > automata.f(best_regle,30))
		{
			copy(Parent[pop-1],Parent[pop-1] + n*n*n,best_regle);
			print_best_solution();
			print_eval();
		}

		//accouplement
		accouplement(pop);

		//vie
		for(int i=0;i<pop;i++)
		{
			swap(regle,Enfant[i]);
			first_improvement(iter);
			swap(regle,Enfant[i]);
		}

		//evaluation
		for(int i=0;i<pop;i++)
		{
			eval_Enfant[i][0] = automata.f(Enfant[i],30);
			eval_Enfant[i][1] = i;
		}
		sort(eval_Enfant.begin(),eval_Enfant.end());

		//selection
		selection(pop);
	}
}

void Fusiller::init_evolution(int pop,int iter)
{
	Parent.resize(pop);
	for(int i=0;i<pop;i++)
	{
		Parent[i] = initialisation();
	}

	Enfant.resize(pop);
	for(int i=0;i<pop;i++)
	{
		Enfant[i] = initialisation();
	}

	Nouvelle_pop.resize(pop);

	eval_Enfant.resize(pop);
	for(int i=0;i<pop;i++)
	{
		eval_Enfant[i].resize(2);
		eval_Enfant[i][1] =i;
	}

	eval_Parent.resize(pop);
	for(int i=0;i<pop;i++)
	{
		eval_Enfant[i].resize(2);
		eval_Enfant[i][1] =i;
	}

	eval_Nouvelle_pop.resize(pop);
	for(int i=0;i<pop;i++)
	{
		eval_Nouvelle_pop[i].resize(2);
	}

	//vie
	for(int i=0;i<pop;i++)
	{
		swap(regle,Parent[i]);
		first_improvement(iter);
		swap(regle,Parent[i]);
	}

	//evaluation initiale
	eval_Parent.resize(pop);
	for(int i=0;i<pop;i++)
	{
		eval_Parent[i].resize(2);
		eval_Parent[i][0] = automata.f(Parent[i],30);
		eval_Parent[i][1] = i;
	}
	sort(eval_Parent.begin(),eval_Parent.end());
}

void Fusiller::accouplement(int pop)
{
	//eugéniste
	for(int i=pop-1;i>=pop/2;i--)
	{
		//choix du partenaire
		int partenaire = (rand()%(pop/2))+pop/2;
		if(partenaire == i)
			partenaire--;
		//accouplement
		for(unsigned int j:utile)
		{
			if(Parent[i][j] != Parent[partenaire][j])
			{
				int choix = rand()%2;
				if(choix == 0)
					Enfant[i][j] = Parent[i][j];
				else
					Enfant[i][j] = Parent[partenaire][j];
			}
			else
			{
				Enfant[i][j] = Parent[i][j];
			}
		}
	}

	//aléatoire
	for(int i=0;i<pop/2;i++)
	{
		//choix du partenaire
		int partenaire = rand()%pop;
		if(partenaire == i)
			partenaire = (partenaire +1)%pop;
		for(unsigned int j:utile)
		{
			if(Parent[i][j] != Parent[partenaire][j])
			{
				int choix = rand()%2;
				if(choix == 0)
					Enfant[i][j] = Parent[i][j];
				else
					Enfant[i][j] = Parent[partenaire][j];
			}
			else
			{
				Enfant[i][j] = Parent[i][j];
			}
		}

	}
	//mutation
	for(int i=0;i<pop;i++)
	{
		float mutation = rand()%100;
		if(mutation <= 20)
		{
			int cellule = rand()%utile.size();
			Enfant[i][utile[cellule]] = rand()%4;
			Enfant[i][utile[cellule]] = rand()%4;
		}
	}
}

void Fusiller::selection(int pop)
{
	int quart_pop = pop/4;
	for(int i=pop-1;i>=quart_pop;i--)
	{
		Nouvelle_pop[i]= Enfant[eval_Enfant[i][1]];
		eval_Nouvelle_pop[i][0] = eval_Enfant[i][0];
		eval_Nouvelle_pop[i][1] = i;
	}
	for(int i = quart_pop-1;i>=0;i--)
	{
		Nouvelle_pop[i] = Parent[eval_Parent[pop-i-1][1]];
		eval_Nouvelle_pop[i][0] = eval_Parent[i+quart_pop][0];
		eval_Nouvelle_pop[i][1] = i;
	}
	sort(eval_Nouvelle_pop.begin(),eval_Nouvelle_pop.end());
	float moyenne =0;
	for(vector<int> i:eval_Nouvelle_pop)
	{
		moyenne += i[0];
	}
	cout<<"moyenne population :" << moyenne/(float)pop<<endl;
	swap(Nouvelle_pop,Parent);
	swap(eval_Nouvelle_pop,eval_Parent);
}

void Fusiller::recuit_simule()
{
	random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0.0, 1.0); //dis(gen)
    float temp = 0.5;
    int nb_mouv;
    while(0.01<temp)
    {
		int nb_mouv = 0;
		for(int i=0;i<100;i++)
		{
			int eval_init = automata.f(regle,20);
			voisin();
			int eval_voisin = automata.f(voisins,20);
			float delta = eval_voisin - eval_init;
			if(delta > 0)
			{
				swap(voisins,regle);
			}
			else
			{
				float u = dis(gen);
				float e = exp(-delta/temp);
				if(u<e)
				{
					nb_mouv++;
					swap(voisins,regle);
				}
			}
		}
		cout<<nb_mouv <<endl;
		temp *= 0.99;
	}
	best_regle_update();
}

void Fusiller::write()
{
	ofstream fichier("../result.txt",ios::out);
	if(fichier)
	{
		for(int i=0;i<n*n*n;i++)
		{
			string str =to_string(best_regle[i]) ;
			fichier << str <<" ";
		}
		fichier.close();
	}
}

void Fusiller::best_regle_update()
{
	copy(regle,regle+n*n*n,best_regle);
}

void Fusiller::print_eval()
{
	cout<<automata.f(best_regle,20)<<endl;
}

void Fusiller::print_regle ()
{
	for(unsigned int i=0;i<n*n*n;i++)
	{
		std::cout<<regle[i] << " " ;
	}
	cout <<std::endl;
}

void Fusiller::print_utile()
{
	for(int i=0;i<utile.size();i++)
	{
		cout<<utile[i] <<"  ";
	}
	cout <<std::endl;
	cout<<utile.size();
}

void Fusiller::print_voisin()
{
	for(int i=0;i<n*n*n;i++)
	{
		cout<<voisins[i]<<" ";
	}
	cout<<endl;
}

void Fusiller::print_best_solution()
{
	for(int i=0;i<n*n*n;i++)
	{
		cout<<best_regle[i]<<" ";
	}
	cout<<endl;
}
