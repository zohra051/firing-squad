#ifndef FUSILLIER_HPP
#define FUSILLIER_HPP

#include "automata.h"
#include <vector>
#include <random>
#include <functional>
#include <random>
#include <cmath>
#include <algorithm>
#include <iterator>
#include <fstream>

using namespace std;

class Fusiller{

	private:
		Automata automata;
		Regle best_regle;
		Regle regle;
		vector<unsigned int> utile;
		Regle voisins;

		//evolution
		vector<Regle> Parent;
		vector<vector<int>> eval_Parent;
		vector<Regle> Enfant;
		vector<vector<int>> eval_Enfant;
		vector<Regle> Nouvelle_pop;
		vector<vector<int>> eval_Nouvelle_pop;

		void indice_utile ();
		Regle initialisation ();
		void best_regle_update();
		void perturbation(int pert);
		void first_improvement(int iter);
		void voisin();

		//evolution
		void init_evolution(int pop,int iter);
		void accouplement(int pop);
		void selection(int pop);

	public:
		Fusiller(Automata automata);


		void ILS(int fi_iteration,int ils_iteration,int perturbation_nb);
		void recuit_simule();
		void algo_evolution(int population,int iteration,int generation);

		void write();

		void print_regle();
		void print_utile();
		void print_eval();
		void print_voisin();
		void print_best_solution();
};

#endif
//pour chaque valeur moyen ecarttype et max
//definir meilleur jeux de param
